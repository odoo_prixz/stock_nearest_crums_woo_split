from odoo import fields, models


class SplitConfiguration(models.Model):
    _name = 'split.configuration'

    qty_start = fields.Float(string="Cantidad inicial", )
    qty_stop = fields.Float(string="Cantidad final", )
    type = fields.Selection([('Local', 'Local'), ('Foraneo', 'Foraneo')], string="Tipo")
    qty_split = fields.Integer(string="Split", )
