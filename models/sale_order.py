from odoo import api, models, fields


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def split_line_for_crums(self, zip_ids, warehouse_ids, split_max, split_real, warehouse_split):
        stock_quant = self.env['stock.quant']
        faltante = self.product_uom_qty
        primero = True
        for zip_id in zip_ids:
            if faltante and split_max > split_real:
                available_qty = stock_quant._get_available_quantity(
                    self.product_id,
                    zip_id.warehouse_id.lot_stock_id,
                    lot_id=False, strict=False
                )
                if available_qty and faltante > 0:
                    diferencia = available_qty - faltante
                    if diferencia <= 0:
                        if not primero:
                            self.create_new_line_qty_need(abs(available_qty), zip_id.warehouse_id)
                        else:
                            self.product_uom_qty = available_qty
                            self.line_warehouse_id = zip_id.warehouse_id.id
                        faltante = abs(diferencia)
                        primero = False
                        if zip_id.warehouse_id not in warehouse_split:
                            warehouse_split += zip_id.warehouse_id
                            split_real += 1
                    else:
                        if not primero:
                            self.create_new_line_qty_need(faltante, zip_id.warehouse_id)
                        else:
                            self.line_warehouse_id = zip_id.warehouse_id.id
                        if zip_id.warehouse_id not in warehouse_split:
                            warehouse_split += zip_id.warehouse_id
                            split_real += 1
                        return split_real, warehouse_split
        warehouse_ids_with_quantity = self.obtiene_almacen_con_cantidades(warehouse_ids, sort=False)
        for warehouse in warehouse_ids_with_quantity:
            if zip_ids:
                break
            if faltante and split_max > split_real:
                diferencia = warehouse['qty'] - faltante
                if diferencia <= 0:
                    if not primero:
                        self.create_new_line_qty_need(warehouse['qty'], warehouse["warehouse_id"])
                    else:
                        self.product_uom_qty = warehouse['qty']
                        self.line_warehouse_id = warehouse["warehouse_id"].id
                    faltante = abs(diferencia)
                    primero = False
                    if warehouse["warehouse_id"] not in warehouse_split:
                        warehouse_split += warehouse["warehouse_id"]
                        split_real += 1
                else:
                    if not primero:
                        self.create_new_line_qty_need(faltante, warehouse["warehouse_id"])
                    else:
                        self.line_warehouse_id = warehouse["warehouse_id"].id
                    if warehouse["warehouse_id"] not in warehouse_split:
                        warehouse_split += warehouse["warehouse_id"]
                        split_real += 1
                    return split_real, warehouse_split
        if faltante and (zip_ids or warehouse_ids_with_quantity):
            if not primero:
                self.create_new_line_qty_need(faltante, zip_ids[0].warehouse_id if zip_ids else warehouse_ids_with_quantity[0]["warehouse_id"])
            else:
                self.line_warehouse_id = zip_ids[0].warehouse_id.id if zip_ids else warehouse_ids_with_quantity[0]["warehouse_id"].id
        return split_real, warehouse_split

    def create_new_line_qty_need(self, qty, warehouse_id):
        values = {
            'order_id': self.order_id.id,
            'name': self.name,
            'product_id': self.product_id.id,
            'product_uom_qty': qty,
            'product_uom': self.product_uom.id,
            'price_unit': self.price_unit,
            'tax_id': [(6, 0, self.tax_id.ids)] if self.tax_id else None ,
        }
        sale_order_line = self.create(values)
        sale_order_line.line_warehouse_id = warehouse_id.id

    def obtiene_almacen_con_cantidades(self, warehouse_ids, sort=False):
        list = []
        for warehouse_id in warehouse_ids:
            available_qty = self.env['stock.quant']._get_available_quantity(
                self.product_id, warehouse_id.lot_stock_id, lot_id=False, strict=False)
            if available_qty:
                list.append({
                    "warehouse_id": warehouse_id,
                    "qty": available_qty
                })
        if sort:
            return sorted(list, key=lambda x: x['qty'], reverse=True)
        else:
            return list


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model_create_multi
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        for record in res:
            if record.order_state_woo:
                split_max = record.get_split_max(record.partner_shipping_id)
                record.splits_orden_por_crums(record.partner_shipping_id, split_max)
            if record.one_delivery_date:
                record.group_by_best_crum()
        return res

    def get_split_max(self, partner_shipping_id):
        partner_zip = partner_shipping_id.zip
        zip_ids = self.env['stock.zip.range'].search([
            ('zip_from', '!=', False),
            ('zip_to', '!=', False),
            ('company_id', '=', self.company_id.id or self.env.context.get('company_id'))
        ]).filtered(lambda l: l.zip_from <= partner_zip.split('-')[0] <= l.zip_to)
        if zip_ids:
            tipo = "Local"
        else:
            tipo = "Foraneo"
        split = self.env["split.configuration"].search([
            ("type", "=", tipo),
            ("qty_start", "<=", self.amount_total),
            ("qty_stop", ">=", self.amount_total)
        ], limit=1)
        if split:
            return split.qty_split
        else:
            return 1

    def set_all_products_best_crum_equals_zips(self, zip_ids):
        zip_warehouse = None
        stock_quant = self.env['stock.quant']
        for zip in zip_ids:
            complete = True
            for line_id in self.order_line.filtered(lambda x: x.product_id.type in ['product']):
                available_qty = stock_quant._get_available_quantity(
                    line_id.product_id,
                    zip.warehouse_id.lot_stock_id,
                    lot_id=False, strict=False
                )
                if available_qty < line_id.product_uom_qty:
                    complete = False
                    break
            if complete:
                zip_warehouse = zip
                break
        if zip_warehouse:
            zips = zip_ids - zip_warehouse
            zip_ids = zip_warehouse + zips
        return zip_ids

    def set_all_products_best_crum_equals_states(self, warehouse_ids):
        warehouse = None
        stock_quant = self.env['stock.quant']
        for warehouse_id in warehouse_ids:
            complete = True
            for line_id in self.order_line.filtered(lambda x: x.product_id.type in ['product']):
                available_qty = stock_quant._get_available_quantity(
                    line_id.product_id,
                    warehouse_id.lot_stock_id,
                    lot_id=False, strict=False
                )
                if available_qty < line_id.product_uom_qty:
                    complete = False
                    break
            if complete:
                warehouse = warehouse_id
                break
        if warehouse:
            states = warehouse_ids - warehouse
            warehouse_ids = warehouse + states
        return warehouse_ids

    def splits_orden_por_crums(self, partner_shipping_id, split_max):
        zip_ids = self.get_zip_ids(partner_shipping_id)
        warehouse_ids = self.get_ware_house_ids(partner_shipping_id)
        split_real = 0
        warehouse_split = self.env["stock.warehouse"]
        for line in self.order_line.filtered(lambda x: x.product_id.type in ['product']):
            split_real, warehouse_split = line.split_line_for_crums(zip_ids, warehouse_ids, split_max, split_real, warehouse_split)

    def get_zip_ids(self, partner_shipping_id):
        partner_zip = partner_shipping_id.zip
        zip_ids = self.env['stock.zip.range'].search([
            ('zip_from', '!=', False),
            ('zip_to', '!=', False),
            ('company_id', '=', self.company_id.id or self.env.context.get('company_id'))
        ]).filtered(lambda l: l.zip_from <= partner_zip.split('-')[0] <= l.zip_to)
        zip_ids = zip_ids.sorted(lambda x: x.priority)
        zip_ids = self.set_all_products_best_crum_equals_zips(zip_ids)
        return zip_ids

    def get_ware_house_ids(self, partner_shipping_id):
        """Regresa los alamcenes que no se encuentren en la lista de zip_ids"""
        partner_state_id = partner_shipping_id.state_id
        state_ids = self.env['stock.state.range'].search([
            ('state_id', '=', partner_state_id.id),
            ('company_id', '=', self.company_id.id)
        ])
        state_ids = state_ids.sorted(lambda x: x.priority)
        warehouse_ids = state_ids.mapped("warehouse_id")
        warehouse_ids = self.set_all_products_best_crum_equals_states(warehouse_ids)
        return warehouse_ids
