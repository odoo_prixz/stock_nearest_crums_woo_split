{
    'name': 'Division de crums para woo',
    'version': '14.0.1.0.0',
    'category': 'Division de crums para crums',
    'summery': 'Division de crums',
    'description': 'Division de pedidos para crums en woocomerce',
    'author': 'prixz',
    'depends': ['woocommerce_odoo_connector', 'stock_nearest_warehouse_acs'],
    'data': [
        "security/ir.model.access.csv",
        "views/split_configuration_views.xml",
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
